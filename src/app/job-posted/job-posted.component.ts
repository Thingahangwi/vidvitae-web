import { element } from 'protractor';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PostJobComponent } from './../post-job/post-job.component';
import {Http,Headers, RequestOptions} from '@angular/http'
import {MatListModule} from '@angular/material';

@Component({
  selector: 'app-job-posted',
  templateUrl: './job-posted.component.html',
  styleUrls: ['./job-posted.component.css']
})


export class JobPostedComponent implements OnInit { 
       
   job = [{
      _jobid: "",
     _title:  "",
     _jodDesc: "",
     _salary: "",
     datePost: "",
     expireDate: "",
     jobStatus:  "",
     location:  "",
   }]; 
    
    companyName: any;
  contactType: any;
  datePosted: any;
  expireDate: any;
  desc: any;
  salary: any;
  sector: any;
  title: any;
    
    jobPost: {};
    question: Array<{}>;

  constructor(private http :Http,private route: Router) { 


   let headers = new Headers({
    'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({
    headers: headers
    });
    
     http.get("https://team5external.thedigitalacademy.co.za/API2/viewJob.php") 
      .subscribe(data => { 
     
      //console.log(data['_body']);    
      // console.log(JSON.parse(data['_body'])[1].Job_ID);
      //  console.log(JSON.parse(data['_body'])[1].Title);
          
          let jobs = JSON.parse(data['_body']);

          for (let index = 0; index < jobs.length; index++) {
            let element = jobs[index];

            this.job.push(
              {
                 '_jobid': element.Job_ID,
                  '_title': element.Title,
                  '_jodDesc': element.Job_Description,
                  '_salary': element.Salary,
                  'datePost': element.Date_Posted,
                  'expireDate': "",
                  'jobStatus':  "",
                  'location': element.Location_Name,
              }
            )   

          }
        
      });
   
  }

  public view(job_Id)
  {
    
    //localStorage.setItem(job_Id,"");

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    let options = new RequestOptions({
      headers: headers
    });


   let jobs = {
      'p_job_id': job_Id
    };

    this.http.post('https://team5external.thedigitalacademy.co.za/API2/viewJobListing.php', jobs,
      options)
      .subscribe(data => {

       // console.log(JSON.parse(data['_body']));
        console.log(data);

        if (JSON.parse(data['_body']) == undefined || JSON.parse(data['_body']) == "") {
          console.log(" No job details found.");

        }
        else {

          let dataObjs = JSON.parse(data['_body']);
          //console.log("Data: " + dataObjs);
 
    
          this.companyName = dataObjs[0].Company_Name;
          this.contactType = dataObjs[0].Contract_Type;
          this.datePosted = dataObjs[0].Date_Posted;
          this.expireDate = dataObjs[0].Expire_Date;
          this.desc = dataObjs[0].Job_Description;
          this.salary = dataObjs[0].Salary;
          this.sector = dataObjs[0].Sector_Name;
          this.title = dataObjs[0].Title;
          this.title = dataObjs[0].Title;
          console.log(this.title);

          this.route.navigateByUrl('/job-details',job_Id);

          for (let index = 0; index < dataObjs.length; index++) {
            let element = dataObjs[index];

            this.question.push({
              'Id': element.Question_ID,
              'question': element.Question_Description
            });

          }
     
        }

      });

  }
  
  
  ngOnInit() {
   
  }
 
}
