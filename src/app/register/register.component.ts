import { LoginComponent } from './../login/login.component';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import {Http,Headers, RequestOptions} from '@angular/http'


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private http :Http, private route: Router) { 
   
  }

  registerUser(firstName,lastname,email,username,password,phone,companyName,companyAddress){
    
      let user = {
      'p_first_name': firstName,
      'p_last_name': lastname,
      'p_email':email,
      'p_username':username,
      'p_password': password,
      'p_contact_number':phone,
      'p_company_name':companyName,
      'p_company_address':companyAddress,
    };

    console.log(user);
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

    let options = new RequestOptions({ headers: headers });

    this.http.post('https://team5external.thedigitalacademy.co.za/API2/recruiter/recruiterRegister.php', user, options)
    .subscribe(data => {
  
      console.log(data);
     
      console.log(data.headers);
      console.log("successful register");
      this.route.navigateByUrl('/login');
  
    })
    ,(error => {
      console.log(error.error); 
      console.log(error.headers);
      console.log("Not registered");
  
    });
  }
  ngOnInit() {
  }

}
