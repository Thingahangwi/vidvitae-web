import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private route: Router) { 

  }

    changePassword(){
    this.route.navigateByUrl('/change-password');
  }

   updateProfile(){
    this.route.navigateByUrl('/update-profile');
  }
  ngOnInit() {
  }

}
