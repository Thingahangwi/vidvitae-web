import { PendingApplicationsComponent } from './../pending-applications/pending-applications.component';
import { JobPostedComponent } from './../job-posted/job-posted.component';
import { LoginComponent } from './../login/login.component';
import { PostJobComponent } from './../post-job/post-job.component';
import { Component, OnInit } from '@angular/core';
import {Http,Headers, RequestOptions} from '@angular/http';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    user = [{
      _firstname: "",
     _surname:  "",
     _jodDesc: "",
     _username: "",
     _password: "",
     _email: "",
     _companyName:  "",
     _companyAddress:  "",
   }]; 

  constructor(private http :Http) { 


   let headers = new Headers({
    'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({
    headers: headers
    });
    
     http.get("https://team5external.thedigitalacademy.co.za/API2/viewJob.php") 
      .subscribe(data => { 

          let jobs = JSON.parse(data['_body']);
          for (let index = 0; index < jobs.length; index++) {
            let element = jobs[index];

            this.user.push(
              {
                 '_firstname':"",
                 '_surname': "",
                '_jodDesc': "",
                '_username': "",
                '_password': "",
                '_email': "",
                '_companyName':  "",
                '_companyAddress':  "",
           }
            )
           
            

          }
        
       
      // this.job._title= JSON.parse(data['_body'])[1].Title;
      //  this.job._jobid=JSON.parse(data['_body'])[1].Job_ID;
      // this.job._jodDesc= JSON.parse(data['_body'])[1].Job_Description;
      //  this.job._salary=JSON.parse(data['_body'])[1].Salary;
      // this.job.datePost=JSON.parse(data['_body'])[1].Date_Posted;
      // this.job.expireDate=JSON.parse(data['_body']).Expire_Date;
      // this.job.jobStatus=JSON.parse(data['_body']).Job_Status;
      // this.job.location=JSON.parse(data['_body'])[1].Location_Name;

      
      });
       
  }


  ngOnInit() {
  }

}
