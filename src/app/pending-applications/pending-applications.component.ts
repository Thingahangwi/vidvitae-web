import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Http,Headers, RequestOptions} from '@angular/http'

@Component({
  selector: 'app-pending-applications',
  templateUrl: './pending-applications.component.html',
  styleUrls: ['./pending-applications.component.css']
})
export class PendingApplicationsComponent implements OnInit {

  video = [{
    _videoUrl:"",
    _question: "",
   }]; 

  constructor(private http :Http) { 
       let headers = new Headers({
    'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({
    headers: headers
    });
    
     this.http.get("https://team5external.thedigitalacademy.co.za/API2/viewAnswerWithQuestion.php") 
      .subscribe(data => { 
     
      console.log(data['_body']);    
     
          
          let videos = JSON.parse(data['_body']);

          for (let index = 0; index < videos.length; index++) {
            let element = videos[index];

            this.video.push(
              {
                 '_videoUrl': element.URL_Reference,
                 '_question': element.Question_Description
              }
            )   

          }
        
      });

  }
    
 videoplayer: any;
 toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
}
  ngOnInit() {
  }

}
