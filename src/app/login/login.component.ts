import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Http,Headers, RequestOptions} from '@angular/http'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    title ;
    subTitle;
   constructor(private route: Router,private http :Http) { }

  ngOnInit() {
  }
   
  loginUser(username,password){
  

     if ( username == "" && password == "" )
      {
      this.subTitle="Please fill up username and password.";
         console.log(this.subTitle);

     }

     else if ( username != "" && password != "" )

     {
           var userCredentials = {

             'p_recruiter_email': username,
              'p_recruiter_password': password
           }

                   let headers = new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded'
                    });
                    let options = new RequestOptions({
                    headers: headers
                    });

                   this.http.post('https://team5external.thedigitalacademy.co.za/API2/recruiter/recruiterLogin.php', userCredentials,
                    options)
                    .subscribe( data => {

                     console.log(JSON.parse(data['_body']));

                       if ( JSON.parse(data['_body']) == undefined || JSON.parse(data['_body']) == "" )
                        {
                            

                             this.title= "Incorrect credentials";
                              this.subTitle= "Please supply correct details.";

                        }
                        else
                        {
                          console.log(JSON.parse(data['_body']));
                          console.log(JSON.parse(data['_body']).Recruiter_ID);
                          console.log(JSON.parse(data['_body']).First_Name);
                          console.log(JSON.parse(data['_body']).Last_Name);
                          console.log(JSON.parse(data['_body']).Email);
                        

                              this.route.navigateByUrl('/dashboard');
                        }

                   });
      }

 }

  }

