
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HttpModule} from '@angular/http'
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { PostJobComponent } from './post-job/post-job.component';
import { JobPostedComponent } from './job-posted/job-posted.component';
//import { CalendarComponent } from './calendar/calendar.component';
import { PendingApplicationsComponent } from './pending-applications/pending-applications.component';
import { ProfileComponent } from './profile/profile.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { JobDetailsComponent } from './job-details/job-details.component';
//import {MdButtonModule , MdCardModule, MdMenuModule, MdToolbarModule, MdIconModule} from '@angular/material';
import {MatListModule} from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    DashboardComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent,
    PostJobComponent,
    JobPostedComponent,
    //CalendarComponent,
    PendingApplicationsComponent,
    ProfileComponent,
    UpdateProfileComponent,
    JobDetailsComponent, 
   
  
  ],
  imports: [MatListModule,
       BrowserModule,BrowserAnimationsModule, FormsModule,HttpModule , RouterModule.forRoot([ 
      { path : 'register', component: RegisterComponent},
      { path : 'profile', component: ProfileComponent},
      { path : 'update-profile', component: UpdateProfileComponent},
       { path : 'change-password', component: ChangePasswordComponent},
       { path : 'login', component: LoginComponent},
       { path: 'forget-password', component: ForgotPasswordComponent },
      { path : 'dashboard', component: DashboardComponent},
      { path : 'post-job', component: PostJobComponent},
        { path : 'job-details', component:  JobDetailsComponent},
      { path : 'job-posted', component: JobPostedComponent},
      { path : 'pending-applications', component: PendingApplicationsComponent},
      { path : '', redirectTo: 'login',pathMatch: 'full'}
      
    ])
    
  ],
  providers: [],
  bootstrap: [AppComponent, LoginComponent,RegisterComponent, ForgotPasswordComponent,DashboardComponent,PostJobComponent,JobPostedComponent, PendingApplicationsComponent,ProfileComponent, ChangePasswordComponent,UpdateProfileComponent]
})
export class AppModule { }
