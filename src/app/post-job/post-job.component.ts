import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import {Http,Headers, RequestOptions} from '@angular/http'

@Component({
  selector: 'app-post-job',
  templateUrl: './post-job.component.html',
  styleUrls: ['./post-job.component.css']
})
export class PostJobComponent implements OnInit {

   public roles = [
    { value: 'Full time', display: 'Full time' },
    { value: 'Part time', display: 'Part time' },
    { value: 'Fixed term', display: 'Fixed term' }
    ];
  
  constructor(public http :Http) { }

    addJob(job_title,description,salary,expireDate,jobTypeId,locationId,recruiterId,sectorId){
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({ headers: headers });

   

      let postJob = {
      'p_title': job_title,
      'p_description': description,
      'p_salary': salary,
      'p_expire_date':expireDate,
      'p_job_type_id':jobTypeId,
      'p_recruiter_id':recruiterId,
      'p_location_id': locationId,
      'p_sector_id':sectorId,
    };
     

     this.http.post("https://team5external.thedigitalacademy.co.za/API2/recruiter/addJob.php", postJob, options)
      .subscribe(data => {
        console.log('job posted  ', data); 
        console.log(postJob);
      }, (error) => {
        console.log('job not added ',error);
      }); 

     
    }

  ngOnInit() {
  }

}
